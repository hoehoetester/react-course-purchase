import React, { Component } from 'react';
import './App.css';

import Coursesales from './Coursesales';

class App extends Component {
  render() {
    let course = [
      { name: 'Basic React and Redux', price: 178 },
      { name: 'Advanced React and Redux', price: 234 },
      { name: 'Basic React Native', price: 150 },
      { name: 'Complete View js', price: 189 },
      { name: 'Responsive Layout', price: 89 },
      { name: 'Basic Angular2/4', price: 177 },
      { name: 'Build Apps with ReactJS', price: 199 }
    ];
    return (
      <div className="App">
        <Coursesales items={course} />
      </div>
    );
  }
}

export default App;
